# Git Workflow for The Good Docs Project

## Overview

Working asynchronously, efficently, and effectively in a gloabally distributed and diverse team takes some planning and coordination to achieve.

This guide explains how to use a Git Branching workflow in GitLab, to contribute changes to projects within the organization.

For this document, we will be using The Good Docs Project '[Git Training Playground](https://gitlab.com/tgdp/git-training/playground)' project for demonstration.

At the highest level, the following actions take place.

1. [Create an issue](#create-an-issue).
2. [Create a new Task Branch and Merge Request from an Issue](#create-a-new-task-branch-and-merge-request-from-an-issue).
3. [Work in the branch and collaborate with others](#work-in-the-branch-and-collaborate-with-others) there as needed.
4. [Review changes in the Merge Request](#review-changes-in-the-merge-request).
5. [Merge the task branch to 'main' branch](#merge-the-task-branch-to-main-branch), closing the issue and deleting the branch.
6. Celebrate the fact that you added value to an important project!

## Before you start

Before you are able to contribute to any project, ensure you have:

- Attended a [Welcome Wagon meeting](https://thegooddocsproject.dev/welcome/) to join the organization.
- Requested and been granted membership to the project(s) that you wish to contribute to.
- Read the [Contributing guide](https://gitlab.com/tgdp/git-training/playground/-/blob/main/CONTRIBUTING.md?ref_type=heads) for that project.
- Familiarity with Git as a tool, and/or have taken the [Git Training](https://gitlab.com/tgdp/git-training) provided by this organization.

## Create an Issue

Creating a [GitLab Issue](https://docs.gitlab.com/ee/user/project/issues/) for a project is a great way to document what you think needs to be changed, and to gather feedback on the proposed changes from existing project members and stakeholders. If you have an Issue assigned to you already, see how to [create a new Task Branch and Merge Request from an Issue](#create-a-new-task-branch-and-merge-request-from-an-issue).

1. Visit a [Project landing page](https://gitlab.com/tgdp/git-training/playground) on GitLab.  

2. Click the Plus Icon in the upper left corner of the workspace.

3. Within the 'In this project' section of the menu, click the 'New issue' option.

![New Issue option in the GitLab 'plus' menu](new_issue.png)

This should take you to the ['New Issue' page](https://gitlab.com/tgdp/git-training/playground/-/issues/new) for that project.

### Describe the changes to be made

1. Fill in the Title, using a few words that summarize the required changes.

2. Add more detail within the Issue Description.

    If this project has an Issue Template, then fill out the requested information.
    Use any screenshots, links to examples, etc. to clearly communicate the changes to be made and the reasoning behind them.
    You may not be the person to make these changes, so this is where you communicate your vision into the mind of another person, help them succeed.

3. Assign it to yourself only if you are confident that the work will be assigned to you. (Optional)

4. Choose the Label for the correct Working Group. (Optional)

    Scoped working group labels start with `wg::` followed by the name of the Working Group this issue falls under.
    If you are unsure, ask one of the project leads.

5. Choose the 'Milestone' that this work will be finished by. (Optional)

6. Click the 'Create issue' button.

## Create a new Task Branch and Merge Request from an Issue

GitLab provides easy to use UI elements to help streamline the workflow with a few simple steps. This process will help you create a 'task branch' and a 'Merge Request' (MR) to merge any changes made in the task branch into the main branch once the MR is approved and merged.

1. On the Issue page, click the 'Create merge request' button.

    ![Create merge request button](create_merge_request.png)

    GitLab will automatically do 3 things:

    - Create a new Task [Branch](https://docs.gitlab.com/ee/user/project/repository/branches/) in the project, with a name in the following format.  
    `<issue number>-<title text>`, Example: `6-example-issue`
    - Take you to the new Merge Request (MR) page.
    - Add the Issue [closing pattern](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically) text to the MR description to automatically close the related issue when the branch is merged into the main branch.

2. Assign the Merge Request to the primary person responsible for the work in the branch.

3. Choose any Reviewers from the project members that should review the work done before it is merged to 'main'.

4. Choose the 'Milestone' that this work will be finished by. (Optional)

5. Check the boxes next to both Merge 'options'.

    - This will Delete the source task branch that is only used to resolve the original Issue.
    - It will also Squash all commits made to the branch into a single 'Merge commit' in the main branch of the project.

    Both of these actions help to keep the project clean and tidy for other project members.

6. Click the 'Create merge request' button.

## Work in the branch and collaborate with others

Now that you have a new Task Branch and Merge Request, you are ready to start making changes to files in the repository to resolve the Issue.

1. In the new Merge Request, click the link to the new task branch below the MR title.

    ![Link to new task branch](task_branch_link.png)

2. After the Repository page loads in the context of the new branch, you can now choose how to edit the files in the branch.

    There are a few editing options that are available to Project members.

    - **Edit a single file only** - This option opens a single file within a webview and allows changes to be made. It does not provide a live preview of the rendered output of those changes. This option is the best for changes to a single file, as it will keep you working in GitLab without switching away to another IDE.

    - **Open the branch in the GitLab IDE** - This option opens a VSCode like interface to work on multiple files at the same time and easily upload new assets into folders of the project branch as needed.  There is also an easy-to-use commit UI for committing changes to the branch. This option is a good general approach for creating or editing multiple markup files.

    - **Open the branch in a GitPod Workspace and IDE** - This option opens a VSCode like interface, like the GitLab IDE, but GitPod is configured on several Project repositories to provide additional functionality, such as a live preview of updates to the Chronologue or Good Docs website. This option is recommended for Chronologue and Website team members.

    - **Open the branch in a Git client on your own computer** - This option is the most complex and requires a fair understanding of Git and the client you are using. It allows significant control over your own editing environment, and can be optimal for some use cases. This approach is only recommended for experienced users.

> For more information on what approach may be best for you, or resources/advice on how to use the above methods, contact your working group lead. 


## Review changes in the Merge Request

Now that changes have been made to the branch associated with your Issue, you can review changes, comment, and suggest additional changes in the associated Merge Request.

1. Navigate to the Merge Request and select the 'Changes' tab.

   ![MR nav bar](mr_changes.png)

   Any changes that have been made to the branch are tracked in this view, including new files, changes to existing files, and deleted files. These changes are compared to the main branch at the time the issue was created.

2. Suggest specific changes to content using the comment icon next to the associated line. Drag the comment icon to attach a comment or suggestion to a range of lines.

   ![Changes comment](git_workflow_changes.png)

3. Add any further comments, or discussion items in the main Merge Request view. 

   This is the best place to discuss the merge request, as Merge Requests are archived and retained after merging.

## Merge the task branch to main branch

A merge request can only be actioned by project members with elevated permissions. These members include the working group leads and tech team members. Contact your working group lead when your merge request is ready, and they will provide an additional review of your changes, inform you or consult you on any additional changes that need to be made, and then merge your branch into main.

## See also

{Include references and/or links to other related documentation such as other how-to guides, conceptual topics, troubleshooting information, and limitation details if any.

- Reference link
- Concept link
- Troubleshooting link}
