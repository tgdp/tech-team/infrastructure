# Infrastructure

Project for tracking infrastructure work and support requests for The Good Docs Project.

## Overview

Changes to be made to technology tools, systems, and resources can be initiated with a new Issue in this project.

This project is also where 'Tech Support' for TGDP can be managed, through a new issue or as an [email to the project issue tracker](mailto:contact-project+tgdp-tech-team-infrastructure-36712398-issue-@incoming.gitlab.com).
