# Change proposal

This document proposes to change the process for initial development, and community review of templates for The Good Docs Project.

## Identified issues

This project has been prorotyped to address several potential issues that have been identified in the current TGDP documentation process.

### Data retention

TGPD currently develops the templates through a fairly standardised process. If we consider an abbreviated process:

1. Templates are requested / identified for development.
2. Templates are researched and developed on Google Docs for collaborative purposes.
3. Community feedback is done on the produced Google Docs.
4. Templates are converted to MD and uploaded to GitLab.

Additionally other TGDP documents such as meeting notes are developed as Google Documents.

It has come to my attention that a significant number of Google Docs are not owned by TGDP and are owned by contributors. An issue may arise where access to these Google Documents is prevented by contributors leaving, or due to changes to their shared documents.

Maintaining the availability of the development documents of the templates allows for development processes to be reviewed at any time in the future, important decision making to be captured, and provides new templateers access to, and visibility over, the process through which the current templates are made.

**Ensuring TGDP has ownership over the Google docs, especially the development documentation for templates, ensures this data retention.**

#### Ease of deployment

To ensure TGDP has ownership over development documentation currently would require specific tech team members with access to TGDP google account to create Google documents to ensure TGDP maintains ownership. This places a significant burden on the tech team.

**Automating this process would ensure the burden on tech team members is reduced.**

### Ease of access

Development documentation is currently discoverable through the issues used to generate templates. With the project growing larger, and currently (2023-03-17) more than 150 issues open, this is not an intuative or easy way to gain access to development documentation.

**Having a central place from which Google Documentation is accessible provides significant ease of access to relevant documentation.**

### Base template

When the base template is established, templateers will need to manually copy the base template to start development. As a manual process, this could be forgotten.

**Automating generation from the base template would ensure the base template is always in use.**

## Other value added

As a Google sheet, additional information can be added to provide context from this centralised location. This could include categorisation, or links to the finalised templates. This may provide significant value for content strategy, and other consumers.

## Potential issues

### Currency of frontmatter / templates

As documents are only generated once, any changes to the base template or frontmatter will not carry through to the final product.

#### Mitigation

The existing process of community review and checklists to be completed before a template is merged should prevent this from being a problem. Additionally, other methods are currently being investigation to ensure currency of frontmatter.

### Abuse

Someone with access to this sheet may be able to maliciously use up space in TGDP Google Drive. This would also apply to anyone with access to a single document in Google Drive.

#### Mitigations

- Google API limits the number of document generation requests avalable daily.
- Script now operates on a single row at a time, forcing a malicious user to generate documents one at a time.
- Google Drive / Document access control can limit access to specific users (can apply to both sheet and generated documents if necessary).

### Migration

Migration of existing documents will be required. This may take some time, and will require a user with access to TGDP Google account to make copies of all existing documents. An automated version of this process may be possible.

# Adoption process

1.  Set up TGDP Drive heirachy (parent folder and '_template' directory with 4 google sheets to be used as templates).
    - Parent folder should have tech team added with Edit permissions in case of issues.
    - Base templates should have specific member edit permissions (tech team initially).
2.  Make copy of Google Sheet so TGDP has ownership.
    - Recommend Google Sheet to be in parent folder for visibility.
    - Remove row 3+ (retain title row and _template row)
    - Change template cells to point to the files made in step 1
3.  Add Google Apps Script to generate documentation in TGDP Drive (look for '// MIGRATION' comments).
    - IDs of the parent folder and templates need to replace those in the Google Apps Script.
    - emails of tech team need to be added to near end of script.
5.  Ensure Google Sheet is properly protected (protect row 1 & 2 with cutom permissions - add tech team and TGDP account)
    - Grant sheet view permissions on link access. 
    - Invite template leads and relevant members to give Edit permissions.
6.  Make copies of all Google Docs currently being used to develop templates.
    - Automation may be needed here to ensure no changes are made to the document before the new document owned by TGDP can be distributed to the relevant templateer(s). It might be easiest to use Google Forms and another Apps Script to generate docs and append a row to the Google Sheet.
7.  Ensure ongoing visibility of sheet so templateers can all contribute to, and learn from, template development documents.