// --------------------------------------------------------------------------------------------
//  TGDP Google Doc Generator
//  -------------------------
//  
//  This script is intended for use with a Google Sheet to generate documents from a series of
//  templates. 
//
//  The code here is very basic, and far from optimal, but it is largely functional and has
//  decent error logging for any edge case issues that arise. The logging is only accessible to
//  those with access to the extensions script.
//
// --------------------------------------------------------------------------------------------
//  TGDP Google Doc Generator
//
//  Current Substitution values:
//
//  - {{content type}}
//
// --------------------------------------------------------------------------------------------
//  Point of contact
//
//  TGDP tech team
//  @secondskoll
// --------------------------------------------------------------------------------------------

function CustomOnEdit(e) {
  var sheet = e.source.getActiveSheet()
  var range = e.range
  var row = range.getRow()
  var column = range.getColumn()
  var values =  SpreadsheetApp.getActive().getSheetByName("Generation").getRange(row, 1, 1, 12);
  var title = values.getCell(1,1).getValue()
  var document = values.getCell(1,2).getValue()
  
  if (sheet.getName() == "Generation") {  

    if (column == 2 && title != "") {


      // Alert that generation has started
      var result = SpreadsheetApp.getUi().alert("Trigger generation?", SpreadsheetApp.getUi().ButtonSet.YES_NO);

      if(result === SpreadsheetApp.getUi().Button.YES) {

        try {
          console.log('Generation triggered.');
          // Trigger function
          SpreadsheetApp.getActive().toast("Please wait, this may take a minute or two. Another message will display when generation is complete.", "Alert", 120);
          console.log('Try to trigger `createDocsFromSheets(row)`.');
          createDocsFromSheets(row);
          SpreadsheetApp.getActive().toast("Generation complete.", "Alert");
        }

        catch(err) {
          console.log('Failed with error %s', err.message);
          error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
          console.log('Current variables: Range ' + range + " Row " + row + " Column " + column);
        }
      }

      else {
        SpreadsheetApp.getActive().toast("Trigger denied.");
      }
    }

    else if (column == 2 && title == "") {
      console.log('No title detected.');
      error = SpreadsheetApp.getUi().alert("No title detected.");
    }

    else if (column == 1) {
      console.log('Edit made to Title.');
      SpreadsheetApp.getActive().toast("Edit made to Title.");
    }

    else if (column == 3) {
      console.log('Edit made to Status / Note.');
      SpreadsheetApp.getActive().toast("Edit made to Status / Note.");
    }
  }

  else {
    console.log('Other cell edited');
    SpreadsheetApp.getActive().toast("Other cell edited.");
    console.log('Active sheet: ' + SpreadsheetApp.getActiveSpreadsheet());
    console.log('Active cell: ' + SpreadsheetApp.getActiveSpreadsheet().getActiveCell());
  }
}

function createDocsFromSheets(row) {

  var range =  SpreadsheetApp.getActive().getSheetByName("Generation").getRange(row, 1, 1, 12);
  var title = range.getCell(1,1).getValue();
  var document = range.getCell(1,2).getValue();
  var status = range.getCell(1,3).getValue();
  var date = range.getCell(1,4).getValue();
  var template = range.getCell(1,5).getValue();
  var userguide = range.getCell(1,6).getValue();
  var process = range.getCell(1,8).getValue();
  var deepdive = range.getCell(1,7).getValue(); 
  var chronologue = range.getCell(1,9).getValue();
  var f_chrono = range.getCell(1,10).getValue();
  var d_chrono = range.getCell(1,11).getValue();
  var other = range.getCell(1,12).getValue();
  
  console.log('createDocsFromSheets triggered.');
  console.log('Key values: Title: {' + title + '} Document: {' + document +'}');
 
  // Use _template files as base for generation of new docs
  const TdocumentTemplate = DriveApp.getFileById('1YR1EalCVdxjT0NJUBznUgqp68NOQ_d-bwS1eJz0-mcY');
  const GdocumentTemplate = DriveApp.getFileById('1GZ4ZyCXncZUJ7ZD3HkjH2XFlKqmIXoKvDstkjMlRQhk');
  const PdocumentTemplate = DriveApp.getFileById('1glDEOvz2iIf8pb-9CxA98LktXPtBIvxUKdVF-jurSy0');
  const RdocumentTemplate = DriveApp.getFileById('1iuZTQEGCiGFbRNYeMcZg9Qkh5v2Y9DJbn4L7VZv-Ndc');
  const OdocumentTemplate = DriveApp.getFileById('1SEM7lnwQSA4Oap1aCOn5AvtT7mRw_VUVior24mrJJiA');
  const CdocumentTemplate = DriveApp.getFileById('1Zc8_zykW7XxMHeShq-L2oVOsSgSwqQujFmW2PqsWl2A');
  const CFdocumentTemplate = DriveApp.getFileById('13SpoTrR_yiFfGhC8VIBKUlx-K1tLIyQChN0lgqgZmyc');
  const CDdocumentTemplate = DriveApp.getFileById('1UZ_HSzDird6L5ejp2tQElVY45i9tqKJOOtpq3DyDwPM');
  
  console.log('Templates loaded.');
 
  const ParentfolderID = "1ngEKdnmtwT6P23p4lZOD9tQ6cD0g9v0e";
  
  var Parentfolder = DriveApp.getFolderById(ParentfolderID)

 
  if (document == "Deliverables") {

    console.log('Template generation triggered.');

    if (template === "" && userguide === "" && process === "" && deepdive === "" && chronologue === "" && f_chrono === "" && d_chrono === "") {

      console.log('Document cells empty.');
        // create subfolder
      var Newfolder = Parentfolder.createFolder(title);
      console.log('New folder created: ' + Newfolder);

      try {
      
        // title contains the template name.
        // Create 4 Google Docs using the template name of the post.
        var Tdocument = TdocumentTemplate.makeCopy(title + ' template', Newfolder);
        var TdocumentID = Tdocument.getId();
       
        var body = DocumentApp.openById(TdocumentID).getBody();
        
        // Append the frontmatter and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);
       
        var Gdocument = GdocumentTemplate.makeCopy(title + ' user guide', Newfolder);
        var GdocumentID = Gdocument.getId();
       
        var body = DocumentApp.openById(GdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var Pdocument = PdocumentTemplate.makeCopy(title + ' process document', Newfolder);
        var PdocumentID = Pdocument.getId();
       
        var body = DocumentApp.openById(PdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);
       
        var Rdocument = RdocumentTemplate.makeCopy(title + ' resources', Newfolder);
        var RdocumentID = Rdocument.getId();
       
        var body = DocumentApp.openById(RdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var Cdocument = CdocumentTemplate.makeCopy(title + ' chronologue example', Newfolder);
        var CdocumentID = Cdocument.getId();
       
        var body = DocumentApp.openById(CdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CFdocument = CFdocumentTemplate.makeCopy(title + ' chronologue friction log example', Newfolder);
        var CFdocumentID = CFdocument.getId();
       
        var body = DocumentApp.openById(CFdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CDdocument = CDdocumentTemplate.makeCopy(title + ' chronologue decision log example', Newfolder);
        var CDdocumentID = CDdocument.getId();
       
        var body = DocumentApp.openById(CDdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);
       
        // Add link based edit permissions
        DriveApp.getFileById(TdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
        DriveApp.getFileById(GdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
        DriveApp.getFileById(PdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);        
        DriveApp.getFileById(RdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
        DriveApp.getFileById(CdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
        DriveApp.getFileById(CFdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
        DriveApp.getFileById(CDdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
       
        // Create the URLs using the Id of the document.
        var TdocumentUrl = `https://docs.google.com/document/d/${TdocumentID}/edit`;
        var GdocumentUrl = `https://docs.google.com/document/d/${GdocumentID}/edit`;
        var PdocumentUrl = `https://docs.google.com/document/d/${PdocumentID}/edit`;
        var RdocumentUrl = `https://docs.google.com/document/d/${RdocumentID}/edit`;
        var CdocumentUrl = `https://docs.google.com/document/d/${CdocumentID}/edit`;
        var CFdocumentUrl = `https://docs.google.com/document/d/${CFdocumentID}/edit`;
        var CDdocumentUrl = `https://docs.google.com/document/d/${CDdocumentID}/edit`;

        // Add this URL to the 4th 5th and 6th columns of the row and add this row
        // to the data to be written back to the sheet.
        template = '=HYPERLINK("' + TdocumentUrl + '", "' + title + ' template")';
        userguide = '=HYPERLINK("' + GdocumentUrl + '", "' + title + ' user guide")';
        process = '=HYPERLINK("' + PdocumentUrl + '", "' + title + ' process document")';
        deepdive = '=HYPERLINK("' + RdocumentUrl + '", "' + title + ' resources")';
        chronologue = '=HYPERLINK("' + CdocumentUrl + '", "' + title + ' chronologue example")';
        f_chrono = '=HYPERLINK("' + CFdocumentUrl + '", "' + title + ' chronologue friction log example")';
        d_chrono = '=HYPERLINK("' + CDdocumentUrl + '", "' + title + ' chronologue decision log example")';
      }

      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }
    }

    else {
      console.log('Document cells already filled. ' + template + userguide + deepdive );
      error = SpreadsheetApp.getUi().alert("Template documents already exist.");
    }
  }

  else if (document == "Meeting notes") {

    console.log('Meeting notes triggered.');

    if (other == "") {

      var Newfolder = Parentfolder.createFolder(title);
      console.log('New folder created: ' + Newfolder);

      try {
   
        var Odocument = OdocumentTemplate.makeCopy(title + ' meeting notes', Newfolder);
        var OdocumentID = Odocument.getId();
       
        var body = DocumentApp.openById(OdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var OdocumentUrl = `https://docs.google.com/document/d/${OdocumentID}/edit`;
        other = '=HYPERLINK("' + OdocumentUrl + '", "' + title + ' meeting notes")';
        console.log('Meeting notes generated and set.');
      }
  
      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }
    }
  
    else {
      console.log('Document cell already filled. ' + other);
      error = SpreadsheetApp.getUi().alert("Meeting notes already exist.");
    }

  }

  else if (document == "Other") {

    console.log('Other document triggered.');

    if (other == "") {

      var Newfolder = Parentfolder.createFolder(title);
      console.log('New folder created: ' + Newfolder);

      try {
   
        var Odocument = OdocumentTemplate.makeCopy(title, Newfolder);
        var OdocumentID = Odocument.getId();

        var body = DocumentApp.openById(OdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var OdocumentUrl = `https://docs.google.com/document/d/${OdocumentID}/edit`;
        other = '=HYPERLINK("' + OdocumentUrl + '", "' + title + ' misc. document")';
        console.log('Other document generated and set.');
      }
  
      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }
    }

    else {
      console.log('Document cell already filled. ' + other);
      error = SpreadsheetApp.getUi().alert("Misc document already exists.");
    }
 }

  else if (document == "Add misc") {

    console.log('Add misc generating.');

    if (other === "" && template != "") {
      try {
        var template = range.getCell(1,5).getFormula();
        var userguide = range.getCell(1,6).getFormula();
        var process = range.getCell(1,8).getFormula();
        var deepdive = range.getCell(1,7).getFormula();
        var chronologue = range.getCell(1,9).getFormula();
        var f_chrono = range.getCell(1,10).getFormula();
        var d_chrono = range.getCell(1,11).getFormula();

        var CopyID = getIDfromURL(template);
        var TFile = DriveApp.getFileById(CopyID);
        var Parentfolders = TFile.getParents();
        console.log('Parent folders ' + Parentfolders);
        while (Parentfolders.hasNext()) {
           var folder = Parentfolders.next();
           console.log(folder.getName());
           var ParentSubfolderID = folder.getId();
           console.log('ParentSubfolderID ' + ParentSubfolderID);
         }

        var ParentSubfolder = DriveApp.getFolderById(ParentSubfolderID);

        var Odocument = OdocumentTemplate.makeCopy(title + " misc", ParentSubfolder);
        var OdocumentID = Odocument.getId();

        var body = DocumentApp.openById(OdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var OdocumentUrl = `https://docs.google.com/document/d/${OdocumentID}/edit`;
        other = '=HYPERLINK("' + OdocumentUrl + '", "' + title + ' misc document")';
        console.log('Misc document generated and set.');
      }
  
      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }
    }

    else {
      console.log('Document cell already filled, or template not available. ' + other, template);
      error = SpreadsheetApp.getUi().alert("Template documents already exist.");
    }

    document = "Deliverables"

  }

  else if (document == "Add chronologue") {

    console.log('Chronologue generating.');

    if (chronologue == "" && f_chrono == "" && d_chrono == "") {
      try {
        var template = range.getCell(1,5).getFormula();
        var userguide = range.getCell(1,6).getFormula();
        var process = range.getCell(1,8).getFormula();
        var deepdive = range.getCell(1,7).getFormula();

        var CopyID = getIDfromURL(template);
        var TFile = DriveApp.getFileById(CopyID);
        var Parentfolders = TFile.getParents();
        console.log('Parent folders ' + Parentfolders);
        while (Parentfolders.hasNext()) {
           var folder = Parentfolders.next();
           console.log(folder.getName());
           var ParentSubfolderID = folder.getId();
           console.log('ParentSubfolderID ' + ParentSubfolderID);
         }

        var ParentSubfolder = DriveApp.getFolderById(ParentSubfolderID);

        var Cdocument = CdocumentTemplate.makeCopy(title + " chronologue example", ParentSubfolder);
        var CdocumentID = Cdocument.getId();

        var body = DocumentApp.openById(CdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CFdocument = CFdocumentTemplate.makeCopy(title + ' chronologue friction log', ParentSubfolder);
        var CFdocumentID = CFdocument.getId();
       
        var body = DocumentApp.openById(CFdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CDdocument = CDdocumentTemplate.makeCopy(title + ' chronologue decision log', ParentSubfolder);
        var CDdocumentID = CDdocument.getId();
       
        var body = DocumentApp.openById(CDdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CdocumentUrl = `https://docs.google.com/document/d/${CdocumentID}/edit`;
        var CFdocumentUrl = `https://docs.google.com/document/d/${CFdocumentID}/edit`;
        var CDdocumentUrl = `https://docs.google.com/document/d/${CDdocumentID}/edit`;
        chronologue = '=HYPERLINK("' + CdocumentUrl + '", "' + title + ' chronologue example")';
        f_chrono = '=HYPERLINK("' + CFdocumentUrl + '", "' + title + ' chronologue friction log example")';
        d_chrono = '=HYPERLINK("' + CDdocumentUrl + '", "' + title + ' chronologue decision log example")';
        console.log('Chronologue documents generated and set.');
      }

      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }

    }

    else if (f_chrono == "" && d_chrono == "") {
      try {
        var template = range.getCell(1,5).getFormula();
        var userguide = range.getCell(1,6).getFormula();
        var process = range.getCell(1,8).getFormula();
        var deepdive = range.getCell(1,7).getFormula();
        var chronologue = range.getCell(1,8).getFormula();

        var CopyID = getIDfromURL(template);
        var TFile = DriveApp.getFileById(CopyID);
        var Parentfolders = TFile.getParents();
        console.log('Parent folders ' + Parentfolders);
        while (Parentfolders.hasNext()) {
           var folder = Parentfolders.next();
           console.log(folder.getName());
           var ParentSubfolderID = folder.getId();
           console.log('ParentSubfolderID ' + ParentSubfolderID);
         }

        var ParentSubfolder = DriveApp.getFolderById(ParentSubfolderID);

        var CFdocument = CFdocumentTemplate.makeCopy(title + ' chronologue friction log', ParentSubfolder);
        var CFdocumentID = CFdocument.getId();
       
        var body = DocumentApp.openById(CFdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CDdocument = CDdocumentTemplate.makeCopy(title + ' chronologue decision log', ParentSubfolder);
        var CDdocumentID = CDdocument.getId();
       
        var body = DocumentApp.openById(CDdocumentID).getBody();
        
        // Append a paragraph and a page break to the document body section directly.
        body.replaceText('{{content type}}', title);

        var CFdocumentUrl = `https://docs.google.com/document/d/${CFdocumentID}/edit`;
        var CDdocumentUrl = `https://docs.google.com/document/d/${CDdocumentID}/edit`;
        f_chrono = '=HYPERLINK("' + CFdocumentUrl + '", "' + title + ' chronologue friction log example")';
        d_chrono = '=HYPERLINK("' + CDdocumentUrl + '", "' + title + ' chronologue decision log example")';
        console.log('Chronologue documents generated and set.');
      }

      catch(err) {
        console.log('Failed with error %s', err.message);
        error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
      }
    }

    else {
      console.log('Document cells already filled. ' + other);
      error = SpreadsheetApp.getUi().alert("Documents already exist.");
    }

    document = "Deliverables"

  }

  else if (document == "Copy existing deliverable(s)") {

    console.log('Copy existing deliverables triggered.');

    if (template != "") {

      var Newfolder = Parentfolder.createFolder(title);
      console.log('New folder created: ' + Newfolder);

      console.log('Template entry found. Copying.' + template);
      var CopyID = getIDfromURL(template)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Tdocument = CopyTemplate.makeCopy(title + ' template', Newfolder);
      var TdocumentID = Tdocument.getId();
      DriveApp.getFileById(TdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);

      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, TdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, TdocumentID, commentId).replyId;
        });
      });
     
      // Create the URLs using the Id of the document.
      var TdocumentUrl = `https://docs.google.com/document/d/${TdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      template = '=HYPERLINK("' + TdocumentUrl + '", "' + title + ' template")';
    }

    else {
      console.log('Document cell already filled. ' + template);
    }

    if (userguide != "") {
      console.log('User Guide entry found. Copying.' + userguide);
      var CopyID = getIDfromURL(userguide)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Gdocument = CopyTemplate.makeCopy(title + ' user guide', Newfolder);
      var GdocumentID = Gdocument.getId();
      DriveApp.getFileById(GdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, GdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, GdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var GdocumentUrl = `https://docs.google.com/document/d/${GdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      userguide = '=HYPERLINK("' + GdocumentUrl + '", "' + title + ' user guide")';
    }

    else {
      console.log('Document cell already filled. ' + userguide);
    }

    if (process != "") {
      console.log('Deep dive entry found. Copying.' + process);
      var CopyID = getIDfromURL(process)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Pdocument = CopyTemplate.makeCopy(title + ' process document', Newfolder);
      var PdocumentID = Pdocument.getId();
      DriveApp.getFileById(PdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, PdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, PdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var PdocumentUrl = `https://docs.google.com/document/d/${PdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      process = '=HYPERLINK("' + PdocumentUrl + '", "' + title + ' Resources")';
    }

    else {
      console.log('Document cell already filled. ' + process);
    }

    if (deepdive != "") {
      console.log('Deep dive entry found. Copying.' + deepdive);
      var CopyID = getIDfromURL(deepdive)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Rdocument = CopyTemplate.makeCopy(title + ' Resources', Newfolder);
      var RdocumentID = Rdocument.getId();
      DriveApp.getFileById(RdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, RdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, RdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var RdocumentUrl = `https://docs.google.com/document/d/${RdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      deepdive = '=HYPERLINK("' + RdocumentUrl + '", "' + title + ' resources")';
    }

    else {
      console.log('Document cell already filled. ' + deepdive);
    }

    if (chronologue != "") {
      console.log('Chronologue entry found. Copying.' + chronologue);
      var CopyID = getIDfromURL(chronologue)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Cdocument = CopyTemplate.makeCopy(title + ' chronologue example', Newfolder);
      var CdocumentID = Cdocument.getId();
      DriveApp.getFileById(CdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, CdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, CdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var CdocumentUrl = `https://docs.google.com/document/d/${CdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      chronologue = '=HYPERLINK("' + CdocumentUrl + '", "' + title + ' resources")';
    }

    if (f_chrono != "") {
      console.log('Chronologue friction log entry found. Copying.' + f_chrono);
      var CopyID = getIDfromURL(f_chrono)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var CFdocument = CopyTemplate.makeCopy(title + ' chronologue friction log', Newfolder);
      var CFdocumentID = CFdocument.getId();
      DriveApp.getFileById(CFdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, CFdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, CFdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var CFdocumentUrl = `https://docs.google.com/document/d/${CFdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      f_chrono = '=HYPERLINK("' + CFdocumentUrl + '", "' + title + ' chronologue friction log")';
    }

    if (d_chrono != "") {
      console.log('Chronologue entry found. Copying.' + chronologue);
      var CopyID = getIDfromURL(chronologue)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var CDdocument = CopyTemplate.makeCopy(title + ' Resources', Newfolder);
      var CDdocumentID = CDdocument.getId();
      DriveApp.getFileById(CDdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
     
      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, CDdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, CDdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var CDdocumentUrl = `https://docs.google.com/document/d/${CDdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      d_chrono = '=HYPERLINK("' + CDdocumentUrl + '", "' + title + ' chronologue decision log")';
    }


    if (other != "") {
      console.log('Other entry found. Copying.' + other);
      var CopyID = getIDfromURL(other)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Odocument = CopyTemplate.makeCopy(title + ' Resources', Newfolder);
      var OdocumentID = Odocument.getId();
      DriveApp.getFileById(OdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);

      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, OdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, OdocumentID, commentId).replyId;
        });
      });

      // Create the URLs using the Id of the document.
      var OdocumentUrl = `https://docs.google.com/document/d/${OdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      other = '=HYPERLINK("' + OdocumentUrl + '", "' + title + ' Other Document")';
    }

    else {
      console.log('Document cell already filled. ' + other);
    }

  date = "Migrated on " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd");
  document = "Deliverables"

  }

  else if (document == "Copy existing document") {

    if (other != "") {

      var Newfolder = Parentfolder.createFolder(title);
      console.log('New folder created: ' + Newfolder);

      console.log('Other entry found. Copying.' + other);
      var CopyID = getIDfromURL(other)
      var CopyTemplate = DriveApp.getFileById(CopyID);
      var Odocument = CopyTemplate.makeCopy(title + ' Resources', Newfolder);
      var OdocumentID = Odocument.getId();
      DriveApp.getFileById(OdocumentID).setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);

      // Added script
      var commentList = Drive.Comments.list(CopyID);
      console.log('Parsing comment list ' + commentList);
      commentList.items.forEach(function(item) {
        var commauthor = item.author.displayName
        var replies = item.replies;
        item.content = "Originally authored by " + commauthor + ".\n\n" + item.content;
        delete item.replies;
        var commentId = Drive.Comments.insert(item, OdocumentID).commentId;
        replies.forEach(function(reply) {
          var commauthor = reply.author.displayName;
          reply.content = "Originally authored by " + commauthor + ".\n\n" + reply.content;
          console.log('Comment: ' + reply + commauthor);
          Drive.Replies.insert(reply, OdocumentID, commentId).replyId;
        });
      });
     
      // Create the URLs using the Id of the document.
      var OdocumentUrl = `https://docs.google.com/document/d/${OdocumentID}/edit`;
     
      // Add this URL to the 4th 5th and 6th columns of the row and add this row
      // to the data to be written back to the sheet.
      other = '=HYPERLINK("' + OdocumentUrl + '", "' + title + ' Other Document")';
    }

    else {
      console.log('Document cell already filled. ' + other);
    }

    date = "Migrated on " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd");
    document = "Other"
  }

  if (date == "") {
    console.log('Date not filled. Setting creation date.');
    var date = Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd");
  }

  else {
    console.log('Date already set.');
  }

  try {
    updatedvalues = [ [ title, document, status, date, template, userguide, deepdive, process, chronologue, f_chrono, d_chrono, other ] ];
    SpreadsheetApp.getActive().getSheetByName("Generation").getRange(row, 1, 1, 12).setValues(updatedvalues);
    console.log('Cell values set. Sheet has updated successfully.');

    var me = SpreadsheetApp.getActive().getOwner();    
    var range_a = SpreadsheetApp.getActive().getSheetByName("Generation").getRange(row, 1, 1, 2);
    var protect_a = range_a.protect().setDescription('Row ' + row + ' protection.');
    var range_b = SpreadsheetApp.getActive().getSheetByName("Generation").getRange(row, 4, 1, 9);
    var protect_b = range_b.protect().setDescription('Row ' + row + ' protection.');

    range_a.setBackground("#D9D9D9");
    range_b.setBackground("#D9D9D9");
      
    protect_a.removeEditors(protect_a.getEditors());
    protect_b.removeEditors(protect_b.getEditors());
    if (protect_a.canDomainEdit()) {
      protect_a.setDomainEdit(false);
    }
    if (protect_b.canDomainEdit()) {
      protect_b.setDomainEdit(false);
    }
    protect_a.addEditor("tgdp-tech-team@googlegroups.com");
    protect_b.addEditor("tgdp-tech-team@googlegroups.com");
    var sheet_editors = protect_a.getEditors();
    console.log('Protection assigned to ' + sheet_editors)

    console.log('Generated cells protected. Sheet has updated successfully.');
  }

  catch(err) {
   console.log('Failed with error %s', err.message);
   error = SpreadsheetApp.getUi().alert("An error has occurred. Please contact the tech team for more information.");
   console.log('Cell values have not been set.');
   console.log('Updated values are: ' + range, title, document, status, template, userguide, process, deepdive, chronologue, other);
  }


}

function getIDfromURL(url) {
  var parts = url.split("/");
  var docID = parts.filter(word => word.length > 30);
  console.log('Cell values have not been set.');
  return docID[0];
}