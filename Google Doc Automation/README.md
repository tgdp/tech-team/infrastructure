# doc-automation overview

This project contains a Google Apps Script which can be used in Google Sheets to automatically generate Google Docs with link based edit permissions.

# Usage

The script has been deployed [here](https://docs.google.com/spreadsheets/d/1W3M9pLVy5EONnXgUFmc4zQbes2LU9JJ-469LwoC1fRI/).

## Base document generation

To generate base documents:

1. Input a Title into a blank row.
2. Select the documents you wish to generate.
   - "Deliverables" generates a document for the "Template", "User Guide", "Resources", "Process", and "Chronologue" columns.
   - "Meeting notes" and "Other" generates a document for the "Other" column.
   - "Copy existing deliverable" creates a copy of any provided documents.
     - Place a link to the Google document(s) in the cells of the row. Ensure the document is viewable with the link provided.
   - "Copy existing document" creates a copy of a provided document.
     - Place a link to the Google document in the "Other" cell. Ensure the document is viewable with the link provided.
   - "Add misc" generates a document in the "Other" column. **It requires the "Template" cell to have already been generated**. 
   - "Add chronologue" generates a document in the "Chronologue" column. **It requires the "Template" cell to have already been generated**.
3. Select "Yes" when prompted to trigger generation.
4. Wait until the columns have filled with hyperlinks to the generated documents.

# Key files

The only file contained in this repository is the Google Apps Script itself.

## createDocsFromSheets

This file is intended to work as a Google Sheets Apps Script.

### Triggers

This script has been set up (in the example sheet) to trigger the `CustomOnEdit` function on any edit of the Google Sheet.
If a cell in the 'Document(s)' column is selected, it then prompts the user as to if the main `createDocsFromSheets` function should
trigger. Edits to any other cells or sheets results in no genration.

The Google Sheet is designed to be properly protected, as editing the URLs of the deployed Google Docs does potentially trigger more doc generation.
Restricting access to relevant personell is recommended.

Significant step based logging has been added for debugging purposes, but the script is now fairly stable and will only update a single row at a time.

### Permissions

This script requires the following permissions:

- See, edit, create, and delete all of your Google Drive files.
    - Required to create Google Drive folders (if no folders are created all documents are left in the root dir), and to apply appropriate edit permissions to generated documents.
- See, edit, create, and delete all your Google Docs documents.
    - Required to create Google Docs in the relevant folders.
- See, edit, create, and delete all your Google Sheets spreadsheets.
    - Required to edit and update the appropriate cells within the sheet.

Auditing by a trusted member of TGDP is recommended before adoption.

# Versions

**1.2**

- Added additional chronologue document options.

**1.1**

- Added Chronologue column and generation options.
- Changed generation options to refer to "Deliverables" rather than "Templates" to support global project usage.

**1.0**

- Generation is triggered on selection of Document drop down.
- Script generates documents accessible to all via link.
- Protection is added to all cells in row save for C (notes) after generation (color added as well).
- Added ability to migrate documents.
- Comments are retained in migration, and the display name of the author is prepended onto the content of the comment.
- Initial generation takes around 20 seconds.
- Migration can take time depending on size of document being migrated.
- Errors will be visible to users, logging to back end is only available to owner.
- Access and protections have been limited to specific google group email addresses.





